import { Container } from "../Container";
import { Search } from "../Search";

import styles from "./Header.module.css";
import logo from "./assets/header-logo.png";

export const Header = () => {
  return (
    <header className={styles.pageHeader}>
      <Container>
        <img src={logo} alt="Logo da M3-Academy" />
        <Search />
      </Container>
    </header>
  );
};
