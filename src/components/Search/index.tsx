import searchIcon from "./assets/search-icon.png";

export const Search = () => {
  return (
    <div>
      <input placeholder="Buscar..." type="text" />
      <button>
        <img src={searchIcon} alt="Icone de busca" />
      </button>
    </div>
  );
};
